<?php

namespace Archaic\Pdo;

use ParagonIE\EasyDB\EasyDB;
use ParagonIE\EasyDB\Factory;

use function Archaic\Log\log;
use function Archaic\PDO\Internal\prepareArgs;

class Pdo {

  private EasyDB $db;

  public function __construct(string $dsn, string $username, string $password) {
    $this->db = Factory::create($dsn, $username, $password);
  }


  /* #region Queries */
  public function query(string $statement, array $args = []): array {
    return $this->db->safeQuery($statement, prepareArgs($args));
  }

  public function record(string $statement, array $args = []): array {
    $result = $this->query($statement, $args);

    if (empty($result)) {
      return [];
    } else {
      return array_shift($result);
    }
  }

  public function value(string $statement, array $args = []) {
    $record = $this->record($statement, $args);

    if ($record == null) {
      return null;
    } else {
      return array_shift($record);
    }
  }
  /* #endregion */

  /* #region Commands */
  
  public function command(string $statement, array $args = []) {
    $this->db->safeQuery($statement, $args);
  }

  public function insert(string $table, array $data) {
    if (empty($data)) {
      log("trying to insert empty data into $table");
      return;
    }

    if (!isset($data[0]) || !is_array($data[0])) {
      $this->db->insert($table, $data);
    } else {
      foreach ($data as $d) {
        $this->db->insert($table, $d);
      }
    }
  }

  public function update(string $table, array $values, array $conditions) {
    $this->db->update($table, $values, $conditions);
  }
  /* #endregion */

  public function transaction(callable $fn) {
    if ($this->db->inTransaction()) {
      $fn();
      $this->db->commit();
      return;
    }

    $this->db->beginTransaction();
    try {
      $fn();
      $this->db->commit();
    } catch(\Exception $e) {
      $this->db->rollBack();
      throw $e;
    }
  }
  
  public function getPDO(): \PDO {
    return $this->db->getPDO();
  }
}
