<?php

namespace Archaic\Pdo\Internal;

function prepareArgs(array $args): array {
  $res = [];

  foreach ($args as $key => $value) {
    if ($key[0] !== ':') {
      $key = ":$key";
    } 
    
    $res[$key] = $value;
  }

  return $res;
}